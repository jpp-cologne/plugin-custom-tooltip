
$(document).ready(function() {

    // taken from http://skfox.com/2008/11/26/jquery-example-inserting-text-with-drag-n-drop/
    $.fn.insertAtCaret = function (myValue) {
        return this.each(function(){
            //IE support
            if (document.selection) {
                this.focus();
                sel = document.selection.createRange();
                sel.text = myValue;
                this.focus();
            }
            //MOZILLA / NETSCAPE support
            else if (this.selectionStart || this.selectionStart == '0') {
                var startPos = this.selectionStart;
                var endPos = this.selectionEnd;
                var scrollTop = this.scrollTop;
                this.value = this.value.substring(0, startPos)+ myValue+ this.value.substring(endPos,this.value.length);
                this.focus();
                this.selectionStart = startPos + myValue.length;
                this.selectionEnd = startPos + myValue.length;
                this.scrollTop = scrollTop;
            } else {
                this.value += myValue;
                this.focus();
            }
        });
    };

    dw.backend.on('sync-option:custom-tooltip', function(args) {
        var chart = args.chart,
            key = args.key,
            customFields = args.option['custom-fields'],
            ui = $('#vis-options-'+key),
            $btn = $('#custom-tooltip-btn-'+key),
            $modal = $('#custom-tooltip-modal-'+key),
            $body = $('.body', $modal),
            $active = $body,
            $fields = $('.fields', $modal);

        $('.title, .body', $modal).on('focus', function() {
            $active = $(this);
        });

        $btn.click(function() {
            $modal.modal();

            var fields = customFields;
            $fields.html('');

            if (!fields) {
                fields = [];
                // update list of available data fields in modal
                var ds = chart.dataset(),
                    fieldMap = {};

                var columns = ds.columns().map(function(c) { return c.name(); });

                // add a fake column for localized map feature name
                if (chart.get('type') == 'maps') {
                    columns.unshift('geoname');
                }

                columns.forEach(function(col) {
                    var key = col.replace(/[^\w0-9]/g, '_'),
                        i = 0;
                    if (/^\d/.test(key)) key = 'X' + key;
                    while (fieldMap[i ? key+i : key]) {
                        i++;
                    }
                    if (i) key = key + i;
                    fieldMap[key] = col;
                    fields.push(key);
                });
            }

            fields.forEach(function(key) {
                $('<li>'+key+'</li>')
                    .click(function() {
                        $active.insertAtCaret('{{ '+$(this).text()+' }}');
                        return false;
                    })
                    .appendTo($fields);
            });
            
            // set the current values for title and body in modal
            var current = chart.get('metadata.visualize.'+key, { title: '', body: '' });

            $('.title', $modal).val(current.title);
            $('.body', $modal).val(current.body);

            // listen to clicks on 'save changes' button in modal
            $('.store', $modal).click(function() {
                // store changes
                chart.set('metadata.visualize.'+key, {
                    title: $('.title', $modal).val(),
                    body: $('.body', $modal).val(),
                    fields: fieldMap
                });
                $modal.modal('hide');
            });

        });


    });

});