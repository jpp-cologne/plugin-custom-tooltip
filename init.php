<?php

// register the map selector dropdown control
DatawrapperHooks::register(
    DatawrapperHooks::VIS_OPTION_CONTROLS,
    function($o, $k) use ($plugin) {
        global $app;
        $env = array('option' => $o, 'key' => $k);
        $app->render('plugins/' . $plugin->getName() . '/custom-tooltip.twig', $env);
    }
);

$plugin->declareAssets(array('custom-tooltip.js'), '#/chart|map/[^/]+/visualize#');
